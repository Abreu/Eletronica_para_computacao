# Eletrônica para computação

## Resumo

Este repositório descreve a realização de dois projetos para a disciplina SSC0180 ‒ Eletrônica para Computação, ministrada pelo professor [Eduardo do Valle Simões](https://gitlab.com/simoesusp) do Instituto de Ciências Matemáticas de Computação da Universidade de São Paulo (ICMC ‒ USP): a construção de uma fonte de tensão ajustável entre 3V e 12V com capacidade de 100mA; e um programa para configurar um Arduino Uno a controlar um display LED para a visualização da saída de um canal de áudio.

# Autores

| Aluno                      | nUSP     |
| -------------------------- | -------- |
| Arthur Ernesto Carvalho    | 14559479 |
| Felipe Carneiro Machado    | 14569373 |
| Guilherme de Abreu Barreto | 12543033 |
| Miguel Rodrigues Tomazini  | 14599300 |
| Thiago Zero Araujo         | 11814183 |

# Sumário

1. Fonte de Tensão
   1. Descrição dos componentes
      1. Diagrama da fonte
      2. Detalhamento das etapas
         1. Conector
         2. Protetor
         3. Transformador
         4. Retificador
         5. Filtro
         6. Resistor
         7. Regulador
         8. Indicador
      3. Lista e tabela de preços
      4. Projeto esquemático de um Circuito Impresso (*Printed Circuit Board ‒ PCB*)
      5. Simulação do seu funcionamento
      6. Imagens e vídeo do trabalho final
         1. *Blueprint* do PCB
         2. Render 3D do PCB
         3. Imagens do protótipo e vídeo do teste deste
2. Visualizador de áudio
    1. Descrição
    2. Lista de componentes

# 1. Fonte de Tensão

## 1.i. Descrição dos componentes

### 1.i.i. Diagrama da fonte

Para que a fonte funcione corretamente, esta necessita executar os seguintes procedimentos para transformar uma corrente alternada (AC) com de 127 V e frequência de 60Hz em uma corrente contínua (DC) passível de ser ajustada entre 3V e 12V, e capacidade de 100 mA:

![diagrama.svg](imgs/diagrama.svg)

### 1.ii. Detalhamento das etapas

#### 1.ii.i. Conector

Componente do circuito responsável pela ligação elétrica, possibilitando a transferência de energia de forma eficiente.

##### Representação

| Esquema elétrico                                                                          | Desenho técnico da placa                               | Modelo 3D                                              |
| ----------------------------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ |
| <img title="" src="imgs/2c74e35decfc286b31733f17f5ea2518cd1ad8aa.svg" alt="" width="600"> | ![](imgs/45d22c9939b934e8be73f2ca665fe0dd0557d03a.png) | ![](imgs/dc62831a992b969f5ebe6328db2e522c49364a77.jpg) |

#### 1.ii.ii. Protetor

Componente responsável por garantir a proteção do circuito, impedindo a passagem de correntes muito altas, prevenindo, dessa forma, incêndios e danos a outros componentes.

##### Representação

| Esquema elétrico                                                                          | Desenho técnico da placa                               | Modelo 3D                                              |
| ----------------------------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ |
| <img title="" src="imgs/3fe5cadbadba5b5edd6910836cf64282dcbed722.svg" alt="" width="600"> | ![](imgs/58620c5acdcfb8c40e11ab968f8212a539c407f6.png) | ![](imgs/41d878c87735fb34240f85acb3ca2483dd575b3c.png) |

#### 1.ii.iii. Transformador

Componente do circuito que limita a tensão de entrada do circuito, de 127 à 15 Volts.

![Gráfico I/s ilustrando a corrente elétrica antes (em vermelho) e após (em amarelo) esta etapa.](imgs/Screenshot_from_2023-06-23_16-07-26.png)

> Gráfico I/s ilustrando a corrente elétrica antes (em vermelho) e após (em amarelo) esta etapa.

##### Representação

| Esquema elétrico                                                                          | Desenho técnico da placa                               | Modelo 3D                                              |
| ----------------------------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ |
| <img title="" src="imgs/faa3e790704fca23e0ec5f65665f84d7d5df23ae.svg" alt="" width="600"> | ![](imgs/a9bb06f57a839c6ca788a6b17c6db8ddbf4e88d2.png) | ![](imgs/4573af50d8a6a844850c462c8d2f62559a1da9ac.png) |

#### 1.ii.iv. Retificador

Componente do circuito que transforma os semiciclos negativos da corrente alternada em positivos, assim “modularizando” a corrente.

![Gráfico I/s ilustrando a corrente antes (em amarelo) e após (em vermelho) esta etapa.](imgs/Screenshot_from_2023-06-23_16-28-44.png)

> Gráfico I/s ilustrando a corrente antes (em amarelo) e após (em vermelho) esta etapa.

A retificação é realizada pelo uso de uma ponte retificadora, composta por diodos. Diodos são dispositivos semi-condutores os quais permitem o fluxo de corrente elétrica apenas em uma direção. Na ponte retificadora os diodos são dispostos de tal maneira a permitir que a corrente em ambas as metades do ciclo alternado seja conduzida para a mesma direção, resultando em uma corrente contínua na saída. À saber, quando a tenção da corrente alternada é positiva em um dos terminais de entrada da ponte retificadora, um dos diodos conduz e permite que a corrente flua para a saída. Enquanto isso, o diodo oposto, que está na outra metade do ciclo, fica inversamente polarizado, bloqueando a passagem da corrente. Segue que quando a tensão da corrente alternada se inverte, a condução dos diodos é invertida.

##### Representação

| Esquema elétrico                                                                          | Desenho técnico da placa                               | Modelo 3D                                              |
| ----------------------------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ |
| <img title="" src="imgs/c430f24142773f349d3f9f3fc279c6cbaa7d1d79.svg" alt="" width="600"> | ![](imgs/19b201a666dcf456e8994df62c862fb56979bf7d.png) | ![](imgs/2a027dcddec367f0fba0051489ee0213395e58ca.png) |

#### 1.ii.v. Filtro

Componente responsável por armazenar carga, liberando corrente quando sua tensão interna for maior que a tensão vinda da fonte, de forma a garantir que a corrente do circuíto fique mais estável. Tal função é desempenhada por um Capacitor, um dispositivo composto por placas condutoras separadas entre si por um isolante dielétrico. Quando uma diferença de potencial é aplicada entre as placas, ocorre a separação de cargas, com cargas positivas acumulando-se em uma placa e cargas negativas na outra. Durante o carregamento de um capacitor, a corrente flui para a placa positiva e é armazenada no campo elétrico, criando uma diferença de potencial *entre as placas*. Quando a corrente é interrompida, o capacitor libera a energia armazenada. A voltagem nas placas faz com que as cargas fluam de volta para neutralizar a separação de cargas, liberando energia elétrica.

![V_Ripple.png](imgs/V_Ripple.png)

[https://pt.wikipedia.org/wiki/Ripple#/media/Ficheiro:V_Ripple.png](https://pt.wikipedia.org/wiki/Ripple#/media/Ficheiro:V_Ripple.png)

Para nosso projeto de fonte, realizamos os seguintes cálculos para aferir a capacitância C necessária para este componente em nosso circuito. Considerando um Ripple de 10%, tensão sob o capacitor de 23.2 V, corrente de saída de 0.112 A, e frequência de 60 Hz, tem-se:

![](imgs/cfe7adbc42e3dd99c67c86304826f7cfbcdc7ae8.png)

##### Representação

| Esquema elétrico                                                                          | Desenho técnico da placa                               | Modelo 3D                                              |
| ----------------------------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ |
| <img title="" src="imgs/5dc979342b206339f4c8d88b027e19d9db3b4b1b.svg" alt="" width="600"> | ![](imgs/983c5076d2785b731f7a7acb30549595bdca9501.png) | ![](imgs/b81bbed71268aebc197c87f15c260ed601f0756c.png) |

#### 1.ii.vi. Resistor

Componente responsável por limitar a passagem de corrente, impedindo que ela assuma valores acima do limite dos componentes.

![Gráfico I/s demonstrando a intensidade de uma corrente em polos opostos de um resistor de 1.5 kΩ. Antes deste (em amarelo) e após este (em vermelho).](imgs/Screenshot_from_2023-06-23_16-25-54.png)

> Gráfico I/s demonstrando a intensidade de uma corrente em polos opostos de um resistor de 1.5 kΩ. Antes deste (em amarelo) e após este (em vermelho).

##### Representação

| Esquema elétrico                                                                          | Desenho técnico da placa                               | Modelo 3D                                              |
| ----------------------------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ |
| <img title="" src="imgs/4a2803712757a7a6a1fd22689db6fb027deaaab0.svg" alt="" width="600"> | ![](imgs/2013bf79f7955f8aa8ea49bd6250d1378b5c69ed.png) | ![](imgs/45aca6a83814a5125786103bcaa0b2badf1aa3e8.jpg) |

#### 1.ii.vii. Regulador

Componente responsável por limitar a tensão máxima do circuito. Tal procedimento é feito pelo uso de um Diodo Zener, posto no circuito com seu cátodo apontando no sentido oposto ao fluxo da corrente. Posto no circuito desta forma, polarizado inversamente, o diodo Zener é capaz de bloquear a passagem de corrente até uma dada tensão, denominada *tensão de ruptura*, a partir da qual a tensão restante o atravessa. Em nosso caso a tensão de ruptura escolhida foi 13 V assim sendo, conseguimos limitar a corrente a atravessar o circuito para os valores pretendidos.

##### Representação

| Esquema elétrico                                                                          | Desenho técnico da placa                               | Modelo 3D                                              |
| ----------------------------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ |
| <img title="" src="imgs/66370b31ddfdbce599463ddfd2173c7497bc888f.svg" alt="" width="600"> | ![](imgs/66b28c36125154656b25aae7e7df1c887bfccaf1.png) | ![](imgs/b81bbed71268aebc197c87f15c260ed601f0756c.png) |

#### 1.ii.viii. Indicador

Componente responsável por indicar a passagem da corrente e, portanto, o funcionamento do circuito. Para tal faz-se uso de um LED um diodo que emite luz ao ser atravessado por uma corrente elétrica.

##### Representação

| Esquema elétrico                                                                          | Desenho técnico da placa                               | Modelo 3D                                              |
| ----------------------------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ |
| <img title="" src="imgs/4e07b569744dca7f2d5e8cdc4c10180098097665.svg" alt="" width="600"> | ![](imgs/6231470cd38b59dde045b534d76e6f1ae9dcbb28.png) | ![](imgs/64c130d9ad9d0cf355331ec42466d69530d4c4fa.jpg) |

## 1.ii. Listagem e tabela de preços

O Transformador e cabos necessários não aparecem listados pois estes foram cordialmente cedidos pelo professor 

| Componente        | Especificação    | Quantidade | Preço | Total     |
| ----------------- | ---------------- | ---------- | ----- | --------- |
| Capacitor         | 680uF x 50V      | 1          | 5,00  | 5,00      |
| Diodo LED         | LED 5mm          | 1          | 0,50  | 0,50      |
| Diodo Retificador | 1N4007           | 10         | 0,20  | 2,00      |
| Diodo Zener       | 13V 1W = 1N4743  | 1          | 0,50  | 0,50      |
| Potenciômetro     | 1W B10K B-16     | 1          | 4,75  | 4,75      |
| Resistor          | 12k 1W           | 1          | 0,40  | 0,40      |
| Resistor          | 5.6k 1W          | 1          | 0,40  | 0,40      |
| Resistor          | 1.5k1W           | 1          | 0,40  | 0,40      |
| Resistor          | 100R 1W          | 1          | 0,40  | 0,40      |
| Transistor        | 2N2222A 60V 0,8A | 1          | 2,00  | 2,00      |
| Conector          | -                | -          | -     | -         |
| **Total**         |                  |            |       | **16,35** |

## 1.iii. Projeto esquemático de um Circuito Impresso (*Printed Circuit Board ‒ PCB*)

![schematic.jpg](imgs/schematic.jpg)

## 1.iv. Simulação do seu funcionamento

Abaixo descrevemos as simulações que conduzimos em antecipação à montagem do protótipo. Nota-se que nestes modelos não se faz presente o fusível, componente protetor do circuíto, tido que os experimentos práticos conduzidos em laboratório garantem que a tensão de entrada não ultrapassa aquela tolerada pelo mesmo.

![circuit-20230623-1552.png](imgs/circuit-20230623-1552.png)

> Imagem da [referente simulação](https://tinyurl.com/2j7ep9fh).

![Protótipo de fonte de tensão (2).png](imgs/prototipo_de_uma_fonte_de_tensao.png)

> Imagem da [referente simulação em Tinkercad](https://www.tinkercad.com/things/bjg8IQnYBWM-prototipo-de-fonte-de-tensao). Esta demonstra apenas a correta disposição dos componentes e o funcionamento do potenciômetro e diodo LED, já que não possui uma fonte de corrente alternada.

## 1.v. Imagens e vídeo do trabalho final

### 1.vi.i *Blueprint do PCB*

<img src="imgs/fonte-de-tensao-brd.svg" title="" alt="fonte-de-tensao-brd.svg" width="690">

### 1.vi.ii *Render* 3D do PCB

![fonte-de-tensao-cima.jpg](imgs/fonte-de-tensao-cima.jpg)

![fonte-de-tensao-frente-1.jpg](imgs/fonte-de-tensao-frente-1.jpg)

![fonte-de-tensao-frente-2.jpg](imgs/fonte-de-tensao-frente-2.jpg)

![fonte-de-tensao-verso.jpg](imgs/fonte-de-tensao-verso.jpg)

### 1.vi.iii. Imagens do protótipo montado e vídeo do teste deste

| Vista      | Superior                                                   | Lateral                                                  |
| ---------- | ---------------------------------------------------------- | -------------------------------------------------------- |
| **Imagem** | ![fonte-de-tensao-superior](imgs/fonte_vista_superior.jpg) | ![fonte-de-tensao-lateral](imgs/fonte_vista_lateral.jpg) |

#### [![Test Video](imgs/test_cover.png)](imgs/fonte_de_tensao.mp4)

> [Vídeo](imgs/fonte_de_tensao.mp4) da realização do teste da fonte.

# 2. Visualizador de áudio

[![Arduino que pisca](imgs/arduino_piscando.jpg)](imgs/arduino_que_pisca.mp4)

> [Vídeo](imgs/arduino_que_pisca.mp4) da realização do teste

## 2.i. Descrição

Matriz de LEDs ligada por 5 cabos de ligação a um Arduino Uno nas entradas (da esquerda para a direita, iniciando pelo cabo amarelo na imagem):

- Cabo amarelo (CLK): conectado ao pin 13;
- Cabo cinza (CS): conectado ao pin 10;
- Cabo vermelho vinho (DIN): conectado ao pin 11;
- Cabo azul (GND): conectado ao GND;
- Verde (VCC): Conectado ao 5V.

O arduino então encontra-se ligado ao computador por um cabo USB-A para USB-B (macho/macho) e uma saída de som por um cabo de ligação TRS ligada na entrada A0 e ao GND. O [programa executado pelo Arduino](arduino_piscante/arduinoPiscante.ino) então lê a saída de áudio e faz brilhar a matriz em um padrão de onda desde o centro, a depender da intensidade do som.

## 2.ii. Lista de Componentes

| Componente | Quantidade |
| --- | --- |
| Arduino Uno | 1 |
| Cabo TRS | 1 |
| Cabo USB-A para USB-B macho-macho | 1 |
| Cabo de ligação macho-fêmea | 5 |
| Matriz de LEDs 1088AS | 1 |

