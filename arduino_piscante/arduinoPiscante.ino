#include <SPI.h>
#include <LedControl.h>

int DIN = 11; 
int CS =  10;
int CLK = 13;

LedControl lc = LedControl(DIN,CLK,CS,0);

int waveFactor = 0;
int isCrescent=0;


void setup() {

Serial.begin(9600);
lc.shutdown(0,false);       //The MAX72XX is in power-saving mode on startup
lc.setIntensity(0,3);       // Set the brightness to maximum value
lc.clearDisplay(0);          // and clear the display

}

void loop() {
  if (isCrescent == 0) waveFactor-=1;
  else waveFactor +=1;

  if (waveFactor == 2) isCrescent = 0;
  else if (waveFactor == -2) isCrescent = 1;

  int random = 0;
  for (int i = 0; i <  50; i++){
    random += analogRead(A0);
  }
  random = random/50;

  random = random%128;
  int led, ledMid, ledSide, ledMidSide;
  switch (random){
    case 0: led = 1; break;
    case 1: led = 3; break;
    case 2: led = 7; break;
    case 3: led = 15; break;
    case 4: led = 31; break;
    case 5: led = 63; break;
    case 6: led = 127; break;
    case 7: led = 255; break;
  }

  ledMid = led;
  ledSide = led;
  ledMidSide = led;

  if (led == 1) ; 
  else switch (waveFactor) {
    case -2: ledMid = (ledMid/4); ledSide = ((ledSide*4) +3); ledMidSide = ((ledMidSide*2) + 1); break;  
    case -1: ledMid = (ledMid/2); ledSide = ((ledSide*2) +1); ledMidSide = ledSide; break;
    case 0: break;
    case 1: ledMid = ((ledMid*2) +1); ledSide = (ledSide/2); ledMidSide = ledSide; break;
    case 2: ledMid = ((ledMid*4) +3); ledSide = (ledSide/4); ledMidSide = (ledMidSide/2); break;
  }

  if (ledSide > 255) ledSide = 255;
  else if (ledSide == 0) ledSide = 1;


  if (ledMidSide > 255) ledMidSide = 255;
  else if (ledMidSide == 0) ledMidSide = 1;

  if (ledMid > 255) ledMid = 255; 
  else if (ledMid == 0) ledMid = 1; 


  byte a[8] = {ledSide, ledMidSide, led, ledMid, ledMid, led, ledMidSide, ledSide};
  printByte(a);

  
  delay(15);
}



void printByte(byte character []) {

int i = 0;
  for(i=0;i<8;i++)
  {
    lc.setRow(0,i,character[i]);
  }

}